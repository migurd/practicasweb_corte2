// declara el objeto alumno
let alumno = {
  "matricula": "20203023",
  "nombre": "Lopez Acosta José Miguel",
  "grupo": "TI-73",
  "carrera": "Tecnologías de la Información",
  "foto": "../img/2021030262.png"
};

let alumnos = {
  "matricula": "2021030262",
  "nombre": "Qui Mora Ángel Ernesto",
  "grupo": "TCI 7-3",
  "carrera": "Tecnologías de la Información",
  "foto": "../img/2021030262.png"
}

console.log(`Matricula: ${alumno.matricula}`);
console.log(`Nombre: ${alumno.nombre}`);

console.log(alumno["nombre"]);
console.log(alumno.nombre);

// objetos compuestos
let cuentaBanco = {
  "numero": "10001",
  "banco": "Banorte",
  cliente: {
    "nombre": "José López",
    "fechaNac": "2020-01-01",
    "sexo": "M"
  },
  "saldo": "10000"
}


console.log(`Nombre: ${cuentaBanco.cliente.nombre}`);
console.log(`Saldo: ${cuentaBanco.saldo}`);

cuentaBanco.cliente.sexo = "F";
console.log(`Sexo: ${cuentaBanco.cliente.sexo}`);

// arreglo de productos
let productos = [
  {
    "codigo": "1001",
    "descripcion": "Atún",
    "precio": "34"
  },
  {
    "codigo": "1002",
    "descripcion": "Jabón de polvo",
    "precio": "23"
  },
  {
    "codigo": "1003",
    "descripcion": "Harina",
    "precio": "43"
  },
  {
    "codigo": "1004",
    "descripcion": "Pasta Dental",
    "precio": "78"
  }
];

// Mostrar un atributo de un objeto del arreglo
for(let i = 0; i < productos.length; i++)
{
  console.log(`${productos[i].codigo} - ${productos[i].descripcion}  -  ${productos[i].precio}`);
}





/* DECLARAR VARIABLES */

const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');

// se declaran los valores
let aside = document.getElementById('aside');
let estado = document.getElementById('status');

// we start the values, so theyre global
let valorAuto_e = document.getElementById('valorAuto');
let pInicial_e = document.getElementById('porcentaje');
let plazos_e = document.getElementById('plazos');
let pagoInicial_e = document.getElementById('pagoInicial'); // e stands for element
let totalFin_e = document.getElementById('totalFin');
let pagoMensual_e = document.getElementById('pagoMensual');

btnCalcular.addEventListener('click', function() {
  // se calculan los valores una vez se presione calcular
  let valorAuto = valorAuto_e.value;
  let pInicial = pInicial_e.value;
  let plazos = plazos_e.value;
  
  // hacer los cálculos
  let pagoInicial = valorAuto * (pInicial / 100);
  let totalFin = valorAuto-pagoInicial;
  let pagoMensual = totalFin / plazos;

  // mostrar los datos
  pagoInicial_e.value = pagoInicial;
  totalFin_e.value = totalFin;
  pagoMensual_e.value = pagoMensual;

  // we update registros and add a new p tag with the data that was sent
  if(
    valorAuto >= 0 && 
    pInicial >= 0 && 
    valorAuto.length > 0 &&
    pInicial.length > 0
    )
  {
    let newParagraph = document.createElement('table');
    newParagraph.innerHTML =
    `
    <thead>
      <th>Dato</th>
      <th>Valor</th>
    </thead>
    <tbody>
      <tr>
        <td>Valor de Auto</td> 
        <td>${valorAuto}</td>
      </tr>
      <tr>
        <td>Porcentaje de Pago Inicial</td>
        <td>${pInicial}</td>
      </tr>
      <tr>
        <td>Plazos</td>
        <td>${plazos} meses</td>
      </tr>
      <tr><td style="background-color: gainsboro; padding: 10px;" colspan="2">Cálculos</td></tr>
      <tr>
        <td>Pago Inicial</td>
        <td>${pagoInicial}</td>
      </tr>
      <tr>
        <td>Total a Financiar</td>
        <td>${totalFin}</td>
      </tr>
      <tr>
        <td>Pago Mensual</td>
        <td>${pagoMensual}</td>
      </tr>
    </tbody>
    `;
    aside.appendChild(newParagraph);
    // we update the status
    estado.style.backgroundColor = 'green';
    estado.style.color = 'white';
    estado.innerHTML = 'Estado: SUCCESS ~ Se guardó el registro';
  }
  else {
    // MOHAHAHAH
    estado.style.backgroundColor = 'red';
    estado.style.color = 'white';
    estado.innerHTML = 'Estado: FAIL ~ Inserte valores válidos';
  }
});

btnLimpiar.addEventListener('click', () => {
  // console.log('limpiando');
  // limpiar outputs 👍
  valorAuto_e.value = '';
  pInicial_e.value = '';
  plazos_e.selectedIndex = 0;
  pagoInicial_e.value = '';
  totalFin_e.value = '';
  pagoMensual_e.value = '';

  // se limpia el status
  estado.style.color = "black";
  estado.style.backgroundColor = "white";
  estado.innerHTML = 'Estado: Se reseteó'

  // se podrían limpiar los registros, pero no
  // aside.innerHTML = ''; -- this aint useful at all lol, itd wipe off the whole thing, even the title, better create a container that contains the registers instead of the whole aside XD
});

/* Se añadió el comentario para ver si netflify funciona :) */
function arribaMouse() {
  parrafo = document.getElementById('pa');
  parrafo.style.color = "#FF00FF";
  parrafo.style.fontSize = "25px";
  parrafo.style.textAlign = "justify";
  parrafo.style.transition = '.2s';
}

function salirMouse() {
  parrafo = document.getElementById('pa');
  parrafo.style.color = "red";
  parrafo.style.fontSize = "17px";
  parrafo.style.textAlign = "left";
}

function limpiar() {
  let parrafo = document.getElementById('pa');
  parrafo.innerHTML="";
}