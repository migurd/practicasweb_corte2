let alumnos = [
  {
    "matricula": "2021030262",
    "nombre": "Qui Mora Ángel Ernesto",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/1.jfif"
  },
  {
    "matricula": "2020030321",
    "nombre": "Ontiveros Govea Yair Alejandro",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/2.jfif"
  },
  {
    "matricula": "2021030136",
    "nombre": "Solis Velarde Oscar Alejandro",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/3.jfif"
  },
  {
    "matricula": "2021030077",
    "nombre": "Arias Tirado Mateo",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/4.jfif"
  },
  {
    "matricula": "2021030266",
    "nombre": "González Ramírez José Manuel",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/5.jfif"
  },
  {
    "matricula": "2021030008",
    "nombre": "Landeros Andrade María Estrella",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/6.jfif"
  },
  {
    "matricula": "2020030714",
    "nombre": "Ruiz Guerrero Axel Jovani",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/7.jfif"
  },
  {
    "matricula": "2020030550",
    "nombre": "Florez Perez Jesús Adolfo",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/8.jfif"
  },
  {
    "matricula": "2021030314",
    "nombre": "Peñaloza Pizarro Felipe Andrés",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/9.jfif"
  },
  {
    "matricula": "2021030108",
    "nombre": "Noriega Fitch Fabio Manuel",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "foto": "../img/alumnos/10.jfif"
  },
];

let profesores = [
  {
    "usuario": "mosuna",
    "nombre": "Osuna Cardenas Melissa",
    "grupo": "TCI 7-3",
    "carrera": "Tecnologías de la Información",
    "puesto": "Tutoria",
    "foto": "../img/alumnos/11.jfif"
  }
];

const alumnosContainer = document.querySelector('.alumnos-container');
const profesoresContainer = document.querySelector('.profesores-container');

for(let i = 0; i < alumnos.length; i++) {
  let newStudent = document.createElement('tr');
  newStudent.innerHTML = 
  `
  <td>${alumnos[i].matricula}</td>
  <td>${alumnos[i].nombre}</td>
  <td>${alumnos[i].grupo}</td>
  <td>${alumnos[i].carrera}</td>
  <td><img src="${alumnos[i].foto}" alt="Alumno ${i+1}"></td>
  `;
  let temp = i + 1;
  if(isEven(i))
  {
    newStudent.style.backgroundColor = 'white';
    newStudent.style.color = 'black';
  }
  else {
    newStudent.style.backgroundColor = '#5D80A3';
    newStudent.style.color = 'white';
  }

  alumnosContainer.appendChild(newStudent);
    
}

for(let i = 0; i < profesores.length; i++) {
  let newTeacher = document.createElement('tr');
  newTeacher.innerHTML = 
  `
  <td>${profesores[i].usuario}</td>
  <td>${profesores[i].nombre}</td>
  <td>${profesores[i].grupo}</td>
  <td>${profesores[i].carrera}</td>
  <td>${profesores[i].puesto}</td>
  <td><img src="${profesores[i].foto}" alt="Alumno ${i+1}"></td>
  `;

  profesoresContainer.appendChild(newTeacher);
    
}

function isEven(n) {
  if(n % 2 === 0) return true;
  else return false;
}