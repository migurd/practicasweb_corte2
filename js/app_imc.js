// botón
const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');

// other thingy
let imgWeight = document.getElementById('imgWeight');

// save values
let peso = document.getElementById('peso');
let altura = document.getElementById('altura');
let imc = document.getElementById('imc');
let edad = document.getElementById('edad');
let edadValue = document.getElementById('edadValue');
let caloriasValue = document.getElementById('caloriasValue');
const radioFemenino = document.querySelector('input[value="femenino"]');
const radioMasculino = document.querySelector('input[value="masculino"]');
let genero = document.getElementById('genero');

// listener
btnCalcular.addEventListener('click', function() {
  // math momento
  let imc_value = (peso.value) / Math.pow(altura.value, 2);

  // assign values 👍
  imc.value = imc_value.toFixed(2);

  // body weight img is updated
  // also the calories
  if(
    peso.value >= 0 &&
    altura.value >= 0 &&
    peso.value.length > 0 &&
    altura.value.length > 0 )
  {
    let imageRoute = getImgRoute(imc_value);
    let tipoPeso = getTipoPeso(imc_value); // this aint needed, but imma keep it anyways 

    imgWeight.src = `${imageRoute}`;
    
    // radio button implementation
    if(radioMasculino.checked)
    {
      genero.innerHTML = 'Calorias a consumir (Hombre)';
      caloriasValue.innerHTML = `${getPesoHombres(edad.value, peso.value)}`;
    }
    if(radioFemenino.checked)
    {
      genero.innerHTML = 'Calorias a consumir (Mujer)';
      caloriasValue.innerHTML = `${getPesoMujeres(edad.value, peso.value)}`;
    }
    edadValue.innerHTML = `${edad.value}`;
  }
  else {
    // MOHAHAHAH
  }

});

btnLimpiar.addEventListener('click', () => {
  // console.log('limpiando');
  // limpiar inputs 👍
  peso.innerHTML = '';
  altura.innerHTML = '';
  imc.innerHTML = '';
  edad.innerHTML = '';
  imgWeight.src = '../img/placeholder.jpg';
  caloriasValue.innerHTML = '';
  genero.innerHTML = 'Calorias a consumir (Género)';
  edadValue.innerHTML = '';

  // se podrían limpiar los registros, pero no
  // aside.innerHTML = ''; -- this aint useful at all lol, itd wipe off the whole thing, even the title, better create a container that contains the registers instead of the whole aside XD
});

function getImgRoute(imc_value)
{
  if(imc_value < 18.5)
    return "../img/01.png";
  else if(imc_value <= 25)
    return "../img/02.png";
  else if(imc_value <= 30)
    return "../img/03.png";
  else if(imc_value <= 35)
    return "../img/04.png";
  else if(imc_value <= 40)
    return "../img/05.png";
  else
    return "../img/06.png";
}

// it wasnt needed at the end lmao
function getTipoPeso(imc_value)
{
  if(imc_value < 18.5)
    return "Peso por debajo de lo normal";
  else if(imc_value <= 25)
    return "Peso saludable";
  else if(imc_value <= 30)
    return "Sobrepeso";
  else if(imc_value <= 35)
    return "Obesidad tipo I";
  else if(imc_value <= 40)
    return "Obesidad tipo II";
  else
    return "Obesidad tipo III";
}

// yes, i know its not a good practice burning numbers, but it is what it is, isnt it?
function getPesoHombres(edad, peso)
{
  let value;
  if(edad < 10)
    return 0;
  if(edad >= 10 && edad < 18)
    value = (17.686 * peso) + 658.2;
  else if(edad < 30)
    value = (15.057 * peso) + 692.2;
  else if(edad < 60)
    value = (11.472 * peso) + 873.1;
  else 
    value = (11.711 * peso) + 587.7;
  return value.toFixed(2);
}

function getPesoMujeres(edad, peso)
{
  let value;
  if(edad < 10)
    return 0;
  if(edad >= 10 && edad < 18)
    value = (13.384 * peso) + 692.6;
  else if(edad < 30)
    value = (14.818 * peso) + 486.6;
  else if(edad < 60)
    value = (8.126 * peso) + 845.6;
  else
    value = (9.082 * peso) + 658.5;
  return value.toFixed(2);
}
